package com.darwinhang.targetcs.clients;

/**
 * This class should be "immutable", probably use builder for it.
 */
public class ProductResponse {
    private String name;
    private final long productId;

    public ProductResponse(long productId){
        this.productId = productId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getProductId() {
        return productId;
    }
}
