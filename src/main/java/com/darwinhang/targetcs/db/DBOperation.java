package com.darwinhang.targetcs.db;

/**
 * Enum used for identification of DB operation that occurred. Cassandra doesn't really
 * separate INSERT/UPDATE, but for this application, I'm considering a newly priceid
 * to be a CREATE.
 */
public enum DBOperation {
    CREATE,
    UPDATE
}
