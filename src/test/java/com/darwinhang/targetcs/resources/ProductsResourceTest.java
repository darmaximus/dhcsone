package com.darwinhang.targetcs.resources;

import com.darwinhang.targetcs.api.Product;
import com.darwinhang.targetcs.clients.PriceClient;
import com.darwinhang.targetcs.clients.ProductClient;
import com.darwinhang.targetcs.clients.ProductResponse;
import com.darwinhang.targetcs.db.DBOperation;
import com.darwinhang.targetcs.db.Price;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

public class ProductsResourceTest {
    private static final ProductClient mockProductClient = mock(ProductClient.class);
    private static final PriceClient mockPriceClient = mock(PriceClient.class);
    private final long expectedId = 12345678;
    private final String expectedName = "foobar";

    private final Product product = new Product();

    @ClassRule
    public static final ResourceTestRule productResource = ResourceTestRule.builder()
            .addResource(new ProductsResource(mockProductClient, mockPriceClient)).build();


    /**
     * Yeah, I know there is a trend saying mocks are evil, but I wanted a way to test the
     * interactions without spinning up a full stack.
     */
    @Before
    public void setup() {
        // create product response to return
        ProductResponse productResponse = new ProductResponse(expectedId);
        productResponse.setName(expectedName);
        when(mockProductClient.fetchProduct(expectedId)).thenReturn(Optional.of(productResponse));

        // create price to return
        Price price = new Price().setCurrencyCode("USD").setValue(BigDecimal.valueOf(12.34));
        when(mockPriceClient.fetchPriceFromDB(expectedId)).thenReturn(Optional.of(price));

        product.setId(expectedId);
        product.setName(expectedName);
        product.setCurrentPrice(price);

        when(mockPriceClient.createOrUpdatePrice(product)).thenReturn(DBOperation.UPDATE);
    }

    @After
    public void tearDown(){
        reset(mockProductClient, mockPriceClient);
    }

    /**
     * Test the get method of the ProductResource returns the expected object, verify appropriate clients both invoked
     */
    @Test
    public void getProductTest() throws Exception {
        assertThat(productResource.target("/products/12345678").request().get(Product.class))
                .isEqualTo(product);
        verify(mockProductClient).fetchProduct(expectedId);
        verify(mockPriceClient).fetchPriceFromDB(expectedId);
    }

    /**
     * Test the case where a Price object has been updated.
     */
    @Test
    public void putProductTest() throws Exception {
        // response.context.status
        int status = Response.Status.NO_CONTENT.getStatusCode();
        assertThat(productResource.target("/products/12345678").request()
                .put(Entity.entity(product, MediaType.APPLICATION_JSON_TYPE)).getStatus()).isEqualTo(status);
        verify(mockPriceClient).createOrUpdatePrice(product);
    }
}
