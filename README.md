## Setup
### Cassandra 
1. Install Docker runtime if not already installed (though, I'm really assuming is installed)
2. pull latest image using `docker pull cassandra`
3. there should be a "datadir" directory in the "myretail" root directory seeded with price data
    a. If that doesn't work, then can run the CQL in the "Notes" section below.
4. run `docker run --name myretail-prices -v /abs/path/to/myretail/datadir:/var/lib/cassandra -d -p 9042:9042 cassandra:latest`
5. wait a second or two. the db server takes some time to start up.

### MyRetail Project
1. Build project `./gradlew build`
2. Run project using option (a) or (b). Make sure db is started.
    a. Use jar that was just built: `java -jar build/libs/QuizService-1.0-SNAPSHOT-all.jar server quiz-service.yaml`
    b. Use gradle application plugin: `./gradlew run`
3. Open Browser and go to localhost:8080/swagger to explore API. You might have to click on the "products" link to expand.

##### Notes
###### CQL to setup DB.
Choosing a primary key - 
```cql
-- copied from docs
CREATE KEYSPACE test
  WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 2 };
  
  
-- Maybe figure out how to store complex Map types, or use User Defined Type
CREATE TABLE test.prices (
  priceID uuid,  -- to allow multiple records for one product, doesn't seem to be 'unique together' constraint
  productID bigint,
  value decimal,
  currency_code varchar,
  PRIMARY KEY (productID, priceID));
  
 INSERT INTO test.prices (priceID, productID, value, currency_code) VALUES (uuid(), 13860428, 10.23, 'USD');
 INSERT INTO test.prices (priceID, productID, value, currency_code) VALUES (uuid(), 12345678, 33.33, 'USD');
```


###### Working productids
- 16696652
- 13860428


## TODO
- [] More unit tests
- [] Integration tests
- [] Use builder for Product
- [] Cassandra dockerfile for easier running of db locally
- [] Application build/run dockerfile/script for running app locally/deployment
- [] deployment pattern to GCP