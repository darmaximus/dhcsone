package com.darwinhang.targetcs.clients;

import com.darwinhang.targetcs.MyRetailServiceConfig;
import com.darwinhang.targetcs.api.Product;
import com.darwinhang.targetcs.db.DBOperation;
import com.darwinhang.targetcs.db.Price;
import com.datastax.driver.core.*;
import com.datastax.driver.core.utils.UUIDs;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class PriceClient {
    private final Session session;
    private final MappingManager manager;
    private final PreparedStatement selectQuery;
    private static final Logger LOG = LoggerFactory.getLogger(PriceClient.class);

    public PriceClient(Cluster cluster, MyRetailServiceConfig config){
        this.session = cluster.connect(config.getCassandraFactory().getKeyspace());
        // TODO: Make the manager global to the application
        this.manager = new MappingManager(session);
        this.selectQuery = session.prepare("SELECT productid, priceid, value, currency_code from prices where productid=?");
    }

    /**
     * Simple method to return Price. Using a prepared statement because won't know the priceid
     * beforehand.
     */
    public Optional<Price> fetchPriceFromDB(long id){
        LOG.info("Starting fetch price from db for product {}", id);
        BoundStatement bs = selectQuery.bind(id);
        ResultSet rs = session.execute(bs);

        Mapper<Price> priceMapper = manager.mapper(Price.class);
        Result<Price> prices = priceMapper.map(rs);
        LOG.info("End fetch price from db for product {}", id);
        return prices.isExhausted() ? Optional.empty() : Optional.of(prices.one());
    }


    /**
     * Determine whether price was created or updated by whether a new UUID was created,
     * basically if an existing item was found.
     */
    public DBOperation createOrUpdatePrice(Product product){
        LOG.info("Starting create or update price from db for product {}", product.getId());
        UUID priceUUID;
        DBOperation dbOperation;

        Optional<Price> existingPrice = fetchPriceFromDB(product.getId());
        if(existingPrice.isPresent()){
            priceUUID = existingPrice.get().getPriceId();
            dbOperation = DBOperation.UPDATE;
        } else {
            priceUUID = UUIDs.random();
            dbOperation = DBOperation.CREATE;
        }
        Mapper<Price> priceMapper = manager.mapper(Price.class);
        Price price = new Price()
                        .setPriceId(priceUUID)
                        .setProductId(product.getId())
                        .setCurrencyCode(product.getCurrentPrice().getCurrencyCode())
                        .setValue(product.getCurrentPrice().getValue());

        priceMapper.save(price);
        LOG.info("Finished create or update price from db for product {}", product.getId());
        return dbOperation;
    }
}
