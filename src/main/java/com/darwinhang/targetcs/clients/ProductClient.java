package com.darwinhang.targetcs.clients;

import com.darwinhang.targetcs.MyRetailServiceConfig;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class ProductClient {
    private final Client client;
    private final MyRetailServiceConfig config;
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final Logger LOG = LoggerFactory.getLogger(ProductClient.class);

    public ProductClient(Client client, MyRetailServiceConfig config){
        this.client=client;
        this.config=config;
    }

    /**
     * Fetch response from product api. Could remove some of the client logic for healthcheck reuse.
     */
    public Optional<ProductResponse> fetchProduct(long productId){
        LOG.info("Starting to fetch product {} from API", productId);
        WebTarget webTarget = client.target(config.getProductURI());
        WebTarget testResourceTarget = webTarget.path(String.valueOf(productId));
        Invocation.Builder invocationBuilder = testResourceTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        ProductResponse pr = new ProductResponse(productId);
        Response.Status statusCode = Response.Status.fromStatusCode(response.getStatus());
        // Should implement retry strategy depending on status code
        switch (statusCode){
            case OK:
                try(InputStream is = response.readEntity(InputStream.class)){
                    pr.setName(extractProductName(is));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                LOG.info("Finished to fetch product {} from API", productId);
                return Optional.of(pr);
            case NOT_FOUND:
                // Could try to set "name" to some internally known error code
                LOG.info("Product {} not found", productId);
                return Optional.empty();
            default:
                LOG.info("Product {} other status code {}", productId, statusCode);
                return Optional.of(pr);
        }
    }


    /**
     * Maybe this should be a part of the response object
     * @return product name
     */
    public static String extractProductName(InputStream is) throws IOException {
        JsonNode root = mapper.readTree(is);
        JsonNode itemNode = root.get("product").get("item");
        JsonNode productDesc = itemNode.get("product_description");
        return productDesc.get("title").asText();
    }
}
