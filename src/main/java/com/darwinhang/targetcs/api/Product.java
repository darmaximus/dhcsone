package com.darwinhang.targetcs.api;

import com.darwinhang.targetcs.db.Price;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;


/**
 * This datastructure represents the interface for the "/products/" endpoint
 */
public class Product {
    private long id;
    private String name;
    private Price currentPrice;

    @JsonProperty
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public Price getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Price currentPrice) {
        this.currentPrice = currentPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id &&
                Objects.equals(name, product.name) &&
                Objects.equals(currentPrice, product.currentPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, currentPrice);
    }
}
