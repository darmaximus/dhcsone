package com.darwinhang.targetcs.clients;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Only testing the helper method here.
 */
public class ProductClientTest {

    @Test
    public void parseProductAPISchemaTest() throws Exception {
        final String expected = "The Big Lebowski (Blu-ray)";
        String productName = ProductClient.extractProductName(getClass().getResourceAsStream("/myRetailProductExample.json"));
        assertEquals(expected, productName);
    }
}
