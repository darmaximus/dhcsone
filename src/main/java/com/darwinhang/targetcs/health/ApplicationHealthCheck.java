package com.darwinhang.targetcs.health;

import com.codahale.metrics.health.HealthCheck;
import com.darwinhang.targetcs.MyRetailServiceConfig;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class ApplicationHealthCheck extends HealthCheck {
    private final Client client;
    private final MyRetailServiceConfig config;

    public ApplicationHealthCheck(Client client, MyRetailServiceConfig config){
        this.client=client;
        this.config=config;
    }

    @Override
    protected Result check() throws Exception {
        // db healthcheck part of dropwizard-cassandra bundle
        // check api available
        WebTarget webTarget = client.target(config.getProductURI());
        WebTarget testResourceTarget = webTarget.path(config.getTestResource());
        Invocation.Builder invocationBuilder = testResourceTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.head();
        if(response.getStatus() != Response.Status.OK.getStatusCode()){
            return Result.unhealthy("Item API returned a non-200 response");
        }
        return Result.healthy();
    }
}
