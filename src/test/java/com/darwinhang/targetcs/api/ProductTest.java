package com.darwinhang.targetcs.api;

import com.darwinhang.targetcs.db.Price;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import io.dropwizard.jackson.Jackson;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test serialization/deserialization, which tests that the provided API is as expected.
 */
public class ProductTest {
    private Product product;

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @BeforeClass
    public static void setUpClass(){
        MAPPER.setPropertyNamingStrategy(
                PropertyNamingStrategy.SNAKE_CASE);
    }

    @Before
    public void setUp(){
        this.product = new Product();
        product.setName("Most Awesome Product");
        product.setId(123456);
        final Price price = new Price().setValue(BigDecimal.valueOf(99.99)).setCurrencyCode("USD");
        product.setCurrentPrice(price);
    }

    @Test
    public void serializeToExpectedJSON() throws Exception {
        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/product.json"), Product.class)
        );

        assertThat(MAPPER.writeValueAsString(product)).isEqualTo(expected);
    }

    @Test
    public void deserializedToExpectedPOJO() throws Exception {
        assertThat(MAPPER.readValue(fixture("fixtures/product.json"), Product.class)).isEqualTo(product);
    }
}
