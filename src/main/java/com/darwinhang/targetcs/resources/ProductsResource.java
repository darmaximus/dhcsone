package com.darwinhang.targetcs.resources;

import com.codahale.metrics.annotation.Timed;
import com.darwinhang.targetcs.db.DBOperation;
import com.darwinhang.targetcs.db.Price;
import com.darwinhang.targetcs.api.Product;
import com.darwinhang.targetcs.clients.PriceClient;
import com.darwinhang.targetcs.clients.ProductClient;
import com.darwinhang.targetcs.clients.ProductResponse;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Optional;
import java.util.concurrent.*;


@Path("/products")
@Api("/products")
@Produces(MediaType.APPLICATION_JSON)
public class ProductsResource {
    final private ProductClient productClient;
    final private PriceClient priceClient;

    public ProductsResource(ProductClient productClient, PriceClient priceClient){
        this.productClient=productClient;
        this.priceClient=priceClient;
    }

    /**
     * Return product name and/or price data if available. Otherwise, return 404.
     * @param id the productid for both API and DB
     * @return The Product merged from Rest API and DB
     */
    @GET
    @Timed
    @Path("/{id}")
    @ApiOperation(value="find products by id", response=Product.class)
    public Product getProduct(@PathParam("id") long id) {
        Product product = new Product();

        ExecutorService executor = Executors.newFixedThreadPool(2);
        Callable<Optional<ProductResponse>> nameTask = () -> productClient.fetchProduct(id);
        Callable<Optional<Price>> priceTask = () -> priceClient.fetchPriceFromDB(id);

        Future<Optional<ProductResponse>> nameFuture = executor.submit(nameTask);
        Future<Optional<Price>> priceFuture = executor.submit(priceTask);

        Optional<ProductResponse> productResponse = Optional.empty();
        Optional<Price> price = Optional.empty();
        try {
            productResponse = nameFuture.get();
            price = priceFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executor.shutdownNow();

//        Optional<ProductResponse> productResponse = productClient.fetchProduct(id);
//        Optional<Price> price = priceClient.fetchPriceFromDB(id);

        if(!productResponse.isPresent() && !price.isPresent()){
            throw new NotFoundException("No product name or price found");
        }

        // use a builder for this instead;
        product.setId(id);
        product.setName(productResponse.orElse(new ProductResponse(id)).getName());
        product.setCurrentPrice(price.orElse(null));

        return product;
    }

    /**
     * Thought about testing if there was an existing resource for the productid from the API,
     * but thought that it might not really matter.
     * @param id the productid for both API and DB
     * @param product Deserialized JSON object provided by user
     * @return 201 (created) or 204 (updated)
     */
    @PUT
    @Timed
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value="set product price by product id", response=Response.class)
    public Response putProduct(@PathParam("id") String id, Product product) {
        DBOperation dbOperation = priceClient.createOrUpdatePrice(product);
        switch (dbOperation) {
            case UPDATE:
                return Response.noContent().build();
            case CREATE:
                return Response.created(URI.create("/proudcts/" + id)).build();
        }
        return Response.notModified().build();
    }
}


