package com.darwinhang.targetcs;

import com.darwinhang.targetcs.clients.PriceClient;
import com.darwinhang.targetcs.clients.ProductClient;
import com.darwinhang.targetcs.health.ApplicationHealthCheck;
import com.darwinhang.targetcs.resources.ProductsResource;
import com.datastax.driver.core.Cluster;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import systems.composable.dropwizard.cassandra.CassandraBundle;
import systems.composable.dropwizard.cassandra.CassandraFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

public class MyRetailServiceApplication extends Application<MyRetailServiceConfig> {

    public static void main(String[] args) throws Exception{
        new MyRetailServiceApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<MyRetailServiceConfig> bootstrap) {
        bootstrap.addBundle(new SwaggerBundle<MyRetailServiceConfig>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(MyRetailServiceConfig configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });
    }

    @Override
    public void run(MyRetailServiceConfig configuration, Environment environment) throws Exception {
        Cluster cluster = configuration.getCassandraFactory().build(environment);

        final Client client = ClientBuilder.newClient();

        // Healthcheck
        final ApplicationHealthCheck appHealthCheck = new ApplicationHealthCheck(client, configuration);
        environment.healthChecks().register("AppHealthCheck", appHealthCheck);

        final ProductClient productClient = new ProductClient(client, configuration);
        final PriceClient priceClient = new PriceClient(cluster, configuration);

        final ProductsResource productsResource = new ProductsResource(productClient, priceClient);
        environment.jersey().register(productsResource);

        // To enforce snake case globally, since that was suggested in example doc
        environment.getObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    }
}
