package com.darwinhang.targetcs.db;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

@Table(name="prices")
public class Price {
    @JsonIgnore
    @PartitionKey
    private long productId;

    @JsonIgnore
    @PartitionKey(1)
    private UUID priceId;

    private BigDecimal value;

    @Column(name="currency_code")
    private String currencyCode;

    public long getProductId() {
        return productId;
    }

    public UUID getPriceId() {
        return priceId;
    }

    @JsonProperty
    public BigDecimal getValue() {
        return value;
    }

    @JsonProperty
    public String getCurrencyCode() {
        return currencyCode;
    }

    public Price setProductId(long productId){
        this.productId=productId;
        return this;
    }

    public Price setPriceId(UUID priceId){
        this.priceId =priceId;
        return this;
    }

    public Price setValue(BigDecimal value){
        this.value=value;
        return this;
    }

    public Price setCurrencyCode(String currencyCode){
        this.currencyCode=currencyCode;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return productId == price.productId &&
                Objects.equals(priceId, price.priceId) &&
                Objects.equals(value, price.value) &&
                Objects.equals(currencyCode, price.currencyCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, priceId, value, currencyCode);
    }
}
