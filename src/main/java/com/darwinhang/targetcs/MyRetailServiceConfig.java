package com.darwinhang.targetcs;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.hibernate.validator.constraints.NotEmpty;
import systems.composable.dropwizard.cassandra.CassandraFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class MyRetailServiceConfig extends Configuration {

    @JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;

    @Valid
    @NotNull
    private CassandraFactory cassandra = new CassandraFactory();

    @JsonProperty("cassandra")
    public CassandraFactory getCassandraFactory() {
        return cassandra;
    }

    @JsonProperty("cassandra")
    public void setCassandraFactory(CassandraFactory cassandra) {
        this.cassandra = cassandra;
    }

    @NotEmpty
    private String productURI;

    @NotEmpty
    private String testResource;

    @JsonProperty
    public String getProductURI() {
        return productURI;
    }
    @JsonProperty
    public void setProductURI(String productURI) {
        this.productURI = productURI;
    }
    @JsonProperty
    public String getTestResource() {
        return testResource;
    }
    @JsonProperty
    public void setTestResource(String testResource) {
        this.testResource = testResource;
    }
}
